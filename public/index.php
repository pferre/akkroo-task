<?php

use LeadRetrieval\Command\CaptureDetailsCommand;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../config.php';

// This file essentially consists of all the controllers
// for the small web application.
// It is built using the Slim PHP microframework
// Documentation for Slim can be found here - https://www.slimframework.com/

// Cheap redirect
$app->get('/', function (Request $request, Response $response) {
    return $response->withRedirect('/lead/add');
});

// Add Lead Page
$app->get('/lead/add', function (Request $request, Response $response) {
    $response = $this->view->render($response, 'leads/add.phtml');

    return $response;
});

// Add Lead Functionality
$app->post('/lead/add', function (Request $request, Response $response) {
    $postData = $request->getParsedBody();

    $command = new CaptureDetailsCommand($postData['leadData']);
    $this->get('bus')->handle($command);

    $response = $this->view->render($response, 'leads/confirm.phtml', ['saved' => true]);

    return $response;
});

$app->run();
