**Instructions**

Please try to use ```php version 7.3``` as it has been developed on such a system.

I've re-jigged the folder structure a little bit from the initial one submitted, 
so to power up the web server, you will need do so via the following directory:

```bash
composer install
cd public && php -S localhost:8000
```
**Tests**  

I've added some tests. Run them from the root directory:

```bash
vendor/bin/phpspec run --format=pretty
```

I'd have wanted to do a more comprehensive coverage with more time, but it does cover both
the processes documented in the task (I hope!).

**My solution**  

I've used the command bus pattern to solve this task and the library I've leveraged is
[Tactician] (https://tactician.thephpleague.com/). 

To add new processes, we simply add a new Middleware and wire it inside the command bus
such as the example below:

```php
$commandBus = new CommandBus(
    [
        new UserSignUp($mailer, $filePersistence),
        new WinnerSelector($winnerGenerator, $mailer, $filePersistence)
    ]
);
```
Due to time constraints, this task does not actually send emails, but prints to the screen,
so if you just run the app you will see a lot of messages on the screen - sorry :-(. 

With a bit more time, I would have used swift mailer, or maybe implemented the Symfony Messenger component.

Out of the box, the app runs the processes as described in the task one after the other, 
so to see them independently, please comment out one of the middlewares above in the config directory.

I would also have liked to use an event based system, to decouple (in this case), sending of emails from the processes.

**Persistence | Data store**

Stores the data to a json file in ```public/model.json```
