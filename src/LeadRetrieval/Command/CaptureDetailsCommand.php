<?php

namespace LeadRetrieval\Command;

class CaptureDetailsCommand
{
    private $firstName;
    private $lastName;
    private $email;
    private $postalCode;
    private $signUp;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->firstName = $data['firstName'];
        $this->lastName = $data['lastName'];
        $this->email = $data['email'];
        $this->postalCode = $data['postalCode'];
        $this->signUp = $data['signUp'];
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @return mixed
     */
    public function getSignUp()
    {
        return $this->signUp;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'postalCode' => $this->postalCode,
            'signUp' => $this->signUp,
            'email' => $this->email,
        ];
    }
}
