<?php

namespace LeadRetrieval\MailDispatch;

class EmailMessage
{
    /**
     * @var string
     */
    private $text;

    /**
     * @param string $text
     */
    public function __construct(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->text;
    }
}
