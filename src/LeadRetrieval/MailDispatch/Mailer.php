<?php

namespace LeadRetrieval\MailDispatch;

class Mailer
{
    /**
     * @param EmailMessage $emailMessage
     */
    public function send(EmailMessage $emailMessage): void
    {
        echo $emailMessage->getMessage();
    }
}
