<?php

namespace LeadRetrieval\Models;

use LeadRetrieval\Save\FilePersister;

class Lead
{
    protected $attributes = [];
    protected static $validFields = [
        'firstName',
        'lastName',
        'email',
        'postalCode',
        'signUp',
    ];
    /**
     * @var FilePersister
     */
    private $filePersister;

    /**
     * Lead constructor.
     * @param FilePersister $filePersister
     */
    public function __construct(FilePersister $filePersister)
    {
        $this->filePersister = $filePersister;
    }

    public function setData(array $attributes = []): void
    {
        foreach ($attributes as $attrKey => $attrValue) {
            if (in_array($attrKey, self::$validFields, true)) {
                $this->set($attrKey, $attrValue);
            }
        }
    }

    public function set($attrKey, $attrValue): void
    {
        $this->attributes[$attrKey] = $attrValue;
    }

    public function save(): void
    {
        $this->filePersister->persist($this->attributes);
    }
}
