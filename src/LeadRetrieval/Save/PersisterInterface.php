<?php

namespace LeadRetrieval\Save;

interface PersisterInterface
{
    /**
     * @param array
     */
    public function persist(array $data);
}
