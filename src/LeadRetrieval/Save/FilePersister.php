<?php

namespace LeadRetrieval\Save;

class FilePersister implements PersisterInterface
{
    /**
     * @param $data
     */
    public function persist(array $data): void
    {
        file_put_contents('model.json', json_encode($data, JSON_PRETTY_PRINT));
    }
}
