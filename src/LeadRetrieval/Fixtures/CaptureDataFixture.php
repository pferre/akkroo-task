<?php

namespace LeadRetrieval\Fixtures;

use LeadRetrieval\Command\CaptureDetailsCommand;

class CaptureDataFixture
{
    public static function loadCaptureData(): CaptureDetailsCommand
    {
        return new CaptureDetailsCommand([
            'firstName' => 'Pierre',
            'lastName' => 'Ferre',
            'postalCode' => 'TW19 7JS',
            'email' => 'test@test.com',
            'signUp' => 'yes',
        ]);
    }
}
