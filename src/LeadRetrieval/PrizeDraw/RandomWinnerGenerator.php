<?php

namespace LeadRetrieval\PrizeDraw;

class RandomWinnerGenerator
{
    private $leadEmails = [
        'pierre@pierreferre.com',
        'test@test.com',
    ];

    /**
     * @return string
     */
    public function pullWinner(): string
    {
        $key = array_rand($this->leadEmails, 1);

        return $this->leadEmails[$key];
    }
}
