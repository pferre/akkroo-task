<?php

namespace LeadRetrieval\Middleware;

use LeadRetrieval\Command\CaptureDetailsCommand;
use LeadRetrieval\MailDispatch\EmailMessage;
use LeadRetrieval\MailDispatch\Mailer;
use LeadRetrieval\Models\Lead;
use LeadRetrieval\PrizeDraw\RandomWinnerGenerator;
use LeadRetrieval\Save\FilePersister;
use League\Tactician\Middleware;

class WinnerSelector implements Middleware
{
    /**
     * @var RandomWinnerGenerator
     */
    private $randomWinnerGenerator;
    /**
     * @var Mailer
     */
    private $mailer;
    /**
     * @var FilePersister
     */
    private $filePersister;

    /**
     * @param RandomWinnerGenerator $randomWinnerGenerator
     * @param Mailer $mailer
     * @param FilePersister $filePersister
     */
    public function __construct(
        RandomWinnerGenerator $randomWinnerGenerator,
        Mailer $mailer,
        FilePersister $filePersister
    ) {
        $this->randomWinnerGenerator = $randomWinnerGenerator;
        $this->mailer = $mailer;
        $this->filePersister = $filePersister;
    }

    /**
     * @param object $command
     * @param callable $next
     *
     * @return mixed
     */
    public function execute($command, callable $next): void
    {
        if ($command instanceof CaptureDetailsCommand && !empty($command->getEmail())) {
            if ($command->getEmail()===$this->randomWinnerGenerator->pullWinner()) {
                $this->mailer->send(
                    new EmailMessage('Congratulations, you have won a prize!')
                );
            } else {
                $this->mailer->send(
                    new EmailMessage('Unlucky! You have been but in a prize draw but did not win!')
                );
            }

            if (!empty($command->getSignUp())) {
                $this->mailer->send(
                    new EmailMessage('Please confirm you want to sign up')
                );
            }

            $lead = new Lead($this->filePersister);
            $lead->setData($command->toArray());
            $lead->save();
        }
    }
}
