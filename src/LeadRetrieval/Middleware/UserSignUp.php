<?php

namespace LeadRetrieval\Middleware;

use LeadRetrieval\Save\FilePersister;
use LeadRetrieval\Command\CaptureDetailsCommand;
use LeadRetrieval\MailDispatch\Mailer;
use LeadRetrieval\MailDispatch\EmailMessage;
use LeadRetrieval\Models\Lead;
use League\Tactician\Middleware;

class UserSignUp implements Middleware
{
    /** @var Mailer */
    private $mailer;
    /** @var FilePersister */
    private $filePersister;

    /**
     * @param Mailer $mailer
     * @param FilePersister $filePersister
     */
    public function __construct(Mailer $mailer, FilePersister $filePersister)
    {
        $this->mailer = $mailer;
        $this->filePersister = $filePersister;
    }

    /**
     * @param object $command
     * @param callable $next
     *
     * @return mixed
     */
    public function execute($command, callable $next): void
    {
        if ($command instanceof CaptureDetailsCommand && !empty($command->getEmail())) {
            $this->mailer->send(
                new EmailMessage('Thank you for signing up')
            );
            $lead = new Lead($this->filePersister);
            $lead->setData($command->toArray());
            $lead->save();
        }

        $next($command);
    }
}
