<?php

namespace spec\LeadRetrieval\Middleware;

use LeadRetrieval\Fixtures\CaptureDataFixture;
use LeadRetrieval\MailDispatch\EmailMessage;
use LeadRetrieval\MailDispatch\Mailer;
use LeadRetrieval\PrizeDraw\RandomWinnerGenerator;
use LeadRetrieval\Save\FilePersister;
use League\Tactician\Middleware;
use PhpSpec\ObjectBehavior;

class WinnerSelectorSpec extends ObjectBehavior
{
    /**
     * @param RandomWinnerGenerator $randomWinnerGenerator
     * @param Mailer $mailer
     * @param FilePersister $filePersister
     */
    public function let(
        RandomWinnerGenerator $randomWinnerGenerator,
        Mailer $mailer,
        FilePersister $filePersister
    ): void {
        $this->beConstructedWith($randomWinnerGenerator, $mailer, $filePersister);
    }

    public function it_is_a_middleware(): void
    {
        $this->shouldBeAnInstanceOf(Middleware::class);
    }

    public function it_calls_a_randomized_winner_generator_and_sends_emails(
        RandomWinnerGenerator $randomWinnerGenerator,
        Mailer $mailer,
        FilePersister $filePersister
    ): void {
        $command = CaptureDataFixture::loadCaptureData();

        $next = static function () {
            return 'callable';
        };

        $randomWinnerGenerator->pullWinner()->shouldBeCalled()->willReturn('test@test.com');
        $mailer->send(new EmailMessage('Congratulations, you have won a prize!'))->shouldBeCalled();
        $mailer->send(new EmailMessage('Please confirm you want to sign up'))->shouldBeCalled();
        $filePersister->persist($command->toArray())->shouldBeCalled();

        $this->execute($command, $next);
    }
}
