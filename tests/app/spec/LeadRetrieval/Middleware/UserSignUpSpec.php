<?php

namespace spec\LeadRetrieval\Middleware;

use LeadRetrieval\Fixtures\CaptureDataFixture;
use LeadRetrieval\MailDispatch\Mailer;
use LeadRetrieval\MailDispatch\EmailMessage;
use LeadRetrieval\Save\FilePersister;
use League\Tactician\Middleware;
use PhpSpec\ObjectBehavior;

class UserSignUpSpec extends ObjectBehavior
{
    public function let(Mailer $mailer, FilePersister $filePersister): void
    {
        $this->beConstructedWith($mailer, $filePersister);
    }

    public function it_is_a_middleware(): void
    {
        $this->shouldBeAnInstanceOf(Middleware::class);
    }

    public function it_processes_data_captured_from_the_application(): void
    {
        $command = CaptureDataFixture::loadCaptureData();

        $next = static function () {
            return 'callable';
        };

        $this->execute($command, $next);
    }

    public function it_sends_an_email_if_email_present(Mailer $mailer, FilePersister $filePersister): void
    {
        $command = CaptureDataFixture::loadCaptureData();

        $next = static function () {
            return 'callable';
        };

        $emailMessage = new EmailMessage('Thank you for signing up');
        $mailer->send($emailMessage)->shouldBeCalled();
        $filePersister->persist($command->toArray())->shouldBeCalled();

        $this->execute($command, $next);
    }
}
