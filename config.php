<?php

use LeadRetrieval\MailDispatch\Mailer;
use LeadRetrieval\Middleware\UserSignUp;
use LeadRetrieval\Middleware\WinnerSelector;
use LeadRetrieval\PrizeDraw\RandomWinnerGenerator;
use LeadRetrieval\Save\FilePersister;
use League\Tactician\CommandBus;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\Locator\InMemoryLocator;
use League\Tactician\Handler\MethodNameInflector\HandleClassNameInflector;
use Slim\App;
use Slim\Views\PhpRenderer;

require 'vendor/autoload.php';

define('ENVIRONMENT', 'development');

date_default_timezone_set('Europe/London');

$config = [
	'displayErrorDetails' => true,
];

$app = new App([
	'settings' => $config
]);

$container = $app->getContainer();

$container['view'] = new PhpRenderer(__DIR__ .'/src/views/');

$locator = new InMemoryLocator();
$handlerMiddleware = new League\Tactician\Handler\CommandHandlerMiddleware(
    new ClassNameExtractor(),
    $locator,
    new HandleClassNameInflector()
);

$mailer = new Mailer();
$winnerGenerator = new RandomWinnerGenerator();
$filePersistence = new FilePersister();
$commandBus = new CommandBus(
    [
        new UserSignUp($mailer, $filePersistence),
        new WinnerSelector($winnerGenerator, $mailer, $filePersistence)
    ]
);

$container['bus'] = $commandBus;
